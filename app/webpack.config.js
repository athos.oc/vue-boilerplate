/*
 * -------------------------------------------
 * adminapp
 * descripcion:
 * autor: cesar.maeso@gmail.com
 * -------------------------------------------
 */
const path = require("path");
const webpack = require("webpack");
const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
  entry: path.resolve(__dirname, "./src/main.js"),
  mode: "production",
  output: {
    path: path.resolve(__dirname, "./public/js"),
    filename: "app.js",
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: [
          {
            loader: "babel-loader"
          },
        ],
      },
      {
        test: /\.scss$/,
        use: ["style-loader", "css-loader", "sass-loader"],
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url-loader",
        query: {
          limit: "10000",
          mimetype: "application/octet-stream",
        },
      },
    ],
  },
  plugins: [
    new VueLoaderPlugin()
  ],
  externals: {
    react: "React",
    ReactDOM: "ReactDOM",
    "react-dom": "ReactDOM",
    // jquery:      'jQuery',
  },
  resolve: {
    modules: [
      path.resolve(__dirname, "./src/js/"),
      path.resolve(__dirname, "./src/js/components"),
      path.resolve(__dirname, "./src/js/modules"),
      path.resolve(__dirname, "./src/js/pages"),
      path.resolve(__dirname, "./src/js/hooks"),
      "node_modules",
    ],
    extensions: ["*", ".js", ".jsx"],
  },
  optimization: {
    // minimize: false,
    minimize: true,

    // Anyhoo, adding optimization.splitChunks.chunks = 'all' is a way of saying “put everything in node_modules
    // into a file called vendors~main.js".
    //runtimeChunk: 'single',
    //splitChunks: {
    //    chunks: 'all',
    //    maxInitialRequests: Infinity,
    //    minSize: 0,
    //    cacheGroups: {
    //        vendor: {
    //            test: /[\\/]node_modules[\\/]/,
    //            name(module) {
    //                // get the name. E.g. node_modules/packageName/not/this/part.js
    //                // or node_modules/packageName
    //                const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
    //
    //                // npm package names are URL-safe, but some servers don't like @ symbols
    //                return `npm.${packageName.replace('@', '')}`;
    //            },
    //        },
    //    },
    //}
  },
};
